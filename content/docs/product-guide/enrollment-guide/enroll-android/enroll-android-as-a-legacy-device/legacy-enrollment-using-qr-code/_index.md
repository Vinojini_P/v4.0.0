---
bookCollapseSection: true
weight: 2
---

# Android Device Legacy Enrollment Using QR Code

Legacy enrollment is deprecated and is designed to take control of the device and data in a BYOD setting.

{{< hint info >}}
<strong>Prerequisites</strong>
<br>
<ul style="list-style-type:disc;">
   <ul style="list-style-type:disc;">
        <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
               <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
               <li>Installing <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> Entgra Agent</a> section should have been followed.</li>
               <li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
               <li>When enrolling a device using QR Code, the the <a  href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/android-configurations/"> Platform Configurations </a> for an Android device should have been first set.</li> 
                 </ul>
    
   
{{< /   hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/YrDfL8IV3ro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Open the Entgra Agent application.</li>
    <li>Click <strong>Continue</strong> after reading the Disclaimer that appears.</li>
    <li>In the screen that follows, click <strong>Enroll with QR Code</strong>.</li>
    <li>In the server, select device ownership as <strong>BYOD</strong></li>
    <li>Scan QR code that is generated on the server.</li>
    <li>Click <strong>Activate</strong> in the device screen.</li>
    <li>Click <strong>Allow</strong> if you agree to the request for permissions for access to the device as indicated. </li>
    <li>Click <strong>Allow</strong> if you agree to using data usage monitoring to allow the server to check the data usage of the device.</li>
    <li>Allow the agent to change <strong>Do not Disturb</strong> status which is used to ring the device.</li>
    <li>Enter and confirm a PIN code, which is required by the administrator to perform any critical tasks with user consent.  Then click <strong>Set PIN Code</strong> to complete the enrollment.</li>
</ul>
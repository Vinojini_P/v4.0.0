---
bookCollapseSection: true
weight: 3
---

# Android Device Enrollment as Work Profile

For Android devices, when you Bring Your Own Device (BYOD) to work, it is possible to set up separate Work Profiles either manually or by using a QR Code. 

A Work Profile allows separation of corporate or work related apps and data from that of the personal ones on your home. Such separation allows your organization to securely and effectively manage work-related apps and data, without restricting or intruding into your personal data on the device. 


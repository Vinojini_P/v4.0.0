---
bookCollapseSection: true
weight: 7
---

# Android Device Enrollment as Google Work Profile

Entgra IoT server is a [Google registered EMM vendor for Work Profiles](https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby). 
Accordingly, it enables integration with Google that allows Entgra to manage user applications within the Work Profile using Google APIs. 

Let us first go through the Work Profile concept and its applications in-depth before proceeding with Google Work Profile enrollment. 

## What is a Work Profile?

A work profile is a containerized space in a personal mobile device that is reserved for corporate use. Employees who bring in their devices to work would prefer to keep their personal apps and data separate from corporate apps and data. To achieve this, Android has a concept known as Work Profiles. A work profile creates a separate logical space in the device  reserved for corporate apps and their data.

## Google Work Profile

The Entgra IoT server enables creating a work profile within a device with control over managing that profile, but not the entire device. This paves for employees to retain their privacy within the device. 

A Google Work Profile is required for managing apps installed inside the work profile. With a Google work profile, you as the customer, Entgra as the EMM vendor and Google as the application provider gets into a binding to provide app management inside the work profile.


### Advantages of Google Work Profile

<ul style="list-style-type:disc;">
    <li>Ability to install apps into the work profile.</li>
    <li>Automatically associate/create a Google account for each work profile user which is used in the Playstore.</li>
    <li>Define what apps will be available/installed for each user.</li>
    <li>Install apps to work profile based on criteria such as battery condition, device idle status and network connectivity conditions.  </li>
    <li>Create a private corporate app store on Google Playstore for organization.</li>
    <li>Define the layout of the Playstore on the user's device.</li>
</ul>

### Auto Generated Google Account (Managed Google Play Accounts) vs Existing Google Accounts(Google Managed Accounts)

Google Work Profile enables creating new/associate existing Google Accounts for each user at enrollment, when separate corporate work profiles are created on the device at enrollment. This account gets added to the Play store automatically, and will be referred to as the **Managed Google Play Account**, here on. If Google accounts exist for all employees in the organization, it is possible to use the same accounts for work profile creation, and such accounts will be referred to as **Google Managed Accounts** here on. 

## Configurations and Prerequisites for Google Work Profile

In order to enroll a device as a Google work profile, there are few prerequisites based on the Google account type that you wish to use. Prior to enrollment, let us look at the necessary configurations.

### Configurations and Prerequisites for Managed Google Play Accounts

{{< hint info >}}
<strong>Prerequisites</strong><br>
<ul style="list-style-type:disc;">
    <li>Request Android for work token and server details from Entgra by writing to contact@entgra.io.</li>
</ul>

<ul style="list-style-type:disc;">
    While you are waiting for the token to arrive,
    <li>Make sure to review the <a href="https://www.android.com/enterprise/terms/">Managed Google Play Agreement</a> as you will have to agree to it during the process.</li>
    <li>Optionally, have the name, email and phone number of following roles ready, if your organization has following roles;
    <ul style="list-style-type:circle;">
    <li>Data protection officer</li>
    <li>EU Representative</li>
    </ul>
    </li>
    <li>Create or have a personal Google Account ready, which is not an account associated with G Suite or other managed domain accounts. Although this is a personal account, its credentials must be shared with your organization. Therefore, it is recommended to create a new account for this purpose.</li>
    <br><b>Important Note</b> <br>
    Once the tokens are received, you only have one hour to complete the process. Therefore, please have the above ready.
</ul>
{{< /   hint >}}

<b>If you have received tokens, </b> let us proceed with the next steps.
<ul style="list-style-type:decimal;">
<li>If the server is not started, start it.</li>
<li>Log in to the Google Account in a new private browsing window.</li>
<li>In the same private browser, log in to the Device Management Console and go to <strong>PLATFORM CONFIGURATION</strong> -> <strong>Android Configurations</strong>.

<img src="../../../../image/2023.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>Under the <strong>Android For Work Configurations</strong> section, fill <strong>Server details provided by EMM vendor</strong> and  <strong>Token</strong> with the value sent by Entgra and click <strong>Begin</strong>.</li>

<li>You will be redirected to <strong>play.google.com</strong>. If you have not signed into the Google account, click <strong>Sign In</strong> to sign in.</li>
<img src="../../../../image/2024.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>Once the logging in process is complete, click <strong>Get Started</strong> on the screen that appears next.</li>
<img src="../../../../image/2026.png" alt="drawing" style="width:75%; border:5pt solid black"/><br/>

<li>Fill in the details regarding the Data Protection Officer and EU Representative if required by your organization. This can be left blank as well, to be filled later. Agree to the Managed Google Play Agreement by clicking <strong>Confirm</strong>.</li>
<img src="../../../../image/2027.png" alt="drawing" style="width:75%; border:5pt solid black"/><br/>

<li>On the screen that appears next, click <strong>Complete Registration</strong>.</li>
<img src="../../../../image/2028.png" alt="drawing" style="width:75%; border:5pt solid black"/><br>

<li>Google will redirect to the EMM server and the following screen will appear to indicate the successful completion of configuration.</li>
<img src="../../../../image/2029.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>If we go to <strong>PLATFORM CONFIGURATION</strong> -> <strong>Android Configurations</strong>, now the <strong>ESA</strong> and <strong>Enterprise ID</strong> field values must be filled. For future reference, please copy both these values to a secure location and save. </li>
<img src="../../../../image/2030.png" alt="drawing" style="width:75%; border:5pt solid black"/>
</ul>

### Enroll a Device

Download and  install the <a href="https://play.google.com/store/apps/details?id=io.entgra.iot.agent">Entgra EMM Agent from Playstore</a>.
Enroll the device similar to how the work profile enrollment was done.

Legacy enrollment is deprecated and is designed to take control of the device and data in a BYOD setting.

{{< hint info >}}
<strong>Prerequisites</strong>
<br>
<ul style="list-style-type:disc;">
    <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
        <li>Logged into the server's<a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/"> Device Management Portal</a>.</li>
        <li>Optionally, basic <a href="{{< param doclink >}}key-concepts/#android ">concepts of Android Device Management</a> will be beneficial as well. </li>
         <li>Make sure to complete the 
    <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/enroll-android-as-a-google-work-profile/#enroll-android-as-a-google-work-profile">Enroll Android as a Google Work Profile</a> section before enrolling devices.
</ul>
{{< /   hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/lirSFp-t-38" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>In the Device Management Portal, Go to <strong>CONFIGURATION MANAGEMENT</strong> -> <strong>PLATFORM CONFIGURATIONS</strong> -> <strong>Android Configurations</strong> section and fill <strong>SERVER_ADDRESS</strong> with the gateway host and the polling interval (60 seconds or more).
</li>
    <li>Download Entgra Agent app from Google Playstore.</li>
    <li>Go to add device section.</li>
    <li>Under _Step 02 - Enroll the Android Agent_, click <strong>Enroll using QR</strong>.</li>
    <li>From the dropdown, select <strong>Google_work_profile</strong></li>
    <li>From the agent, select <strong>Continue</strong> -> <strong>Enroll with QR Code</strong> -> <strong>Scan</strong> and scan the QR code. Follow the on-screen instructions for enrolling.</li>
</ul>

Note that any assigned apps will be automatically installed.















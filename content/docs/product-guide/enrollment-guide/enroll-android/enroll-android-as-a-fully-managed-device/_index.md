---
bookCollapseSection: true
weight: 4
---

# Android Device Enrollment as a Fully Managed

Fully Managed type of enrollment is ideal for COPE (corporately-owned personally-enabled) devices where the organization gives out devices for its employees. 
With granular control over device data and security, and access to Android's full suite of app management features, this type of enrollment offers comprehensive device management capabilities.

Some of the available features include setting the minimum password requirements on the device, ability to remotely wipe and lock the device, and setting up default responses to app permission requests. It is also possible to remotely install and/or remove apps with this type of enrollment.

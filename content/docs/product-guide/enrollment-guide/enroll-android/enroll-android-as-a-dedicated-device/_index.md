---
bookCollapseSection: true
weight: 5
---

# Android Device Enrollment as a Dedicated Device

The Dedicated type of Android Device Enrollment, also known as COSU (Corporately-Owned Single-Use), is widely used with fully managed devices that serve a specific purpose. Examples of this type of usage include employee-facing device usage  such as in inventory management, field service management, transport, logistics as well as customer-facing device usages like kiosks, digital signage, hospitality check-in etc.

Some of the significant features of Dedicated devices are:
<ul style="list-style-type:decimal;">
<li>Lock down devices to specific whitelisted apps</li>
<li>Block users from escaping locked down devices to enable other actions.</li>
<li>Set lock screen restrictions </li>
<li>Device sharing between multiple users (such as shift workers or public-kiosk users) </li>
<li>Suspend over-the-air (OTA) system updates over critical periods by freezing the OS version.</li>
<li>Remotely wipe and lock work data</li>
<li>Automatic compliance enforcement</li>
<li>Lock app(s) to screen</li>
<li>Automatic compliance enforcement</li>
<li>Distribute apps silently</li>
</ul>
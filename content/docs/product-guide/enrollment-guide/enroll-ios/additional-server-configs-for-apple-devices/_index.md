---
bookCollapseSection: true
weight: 1
---

# Additional Server Configurations for iOS

This section takes you through the additional server configurations required for setting up an iOS device. 

Apple server configurations, both for iOS and Mac OS are given here in detail along with the platform configurations, based on the required enrollment method.

The configurations necessary for enrolling the device at the Apple DEP (Device Enrollment Program), for both iO and Mac OS, are listed in the section that follows. 


---
bookCollapseSection: true
weight: 1
---

# Apple(iOS and MacOS) Server Configurations

Prior to enrolling an iOS device, there are some configurations that need to be done on the server.

Although iOS is a supported, device type of Entgra IoT server, by default it is not bundled with the server. 

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li>If you have not requested for iOS plugin before, please <a href="https://entgra.io/contact">contact Entgra</a> to get a copy. If MacOS support is needed in the plugin, please mention this in the description.</li>
    <li>As  mentioned in system requirements, maven is needed to install the plugin.</li>
</ul>
{{< /hint >}}

## Install iOS plugin
iOS device plugin is not bundled with the server as out-of-the-box and as mentioned in prerequisites, it has to be requested from Entgra. A zip file containing the installable will be sent by Entgra and this can be installed with Maven. Click bellow to see the steps,
{{< expand "Steps" "..." >}}

1. Copy the iOS plugin zip mentioned in the prerequisites to <strong>IOT_HOME</strong> folder and extract.
2. Using command prompt, goto <strong>IOT_HOME/ios-feature-deployer/</strong> folder
3. Execute the following command ```mvn clean install -f ios-feature-deployer.xml```
4. When the following question is asked <strong>"Do you agree? (y,n)"</strong> Type <strong>y</strong> and Enter to agree. Else, <strong>n</strong> to abort.
{{< /expand >}}

## Run iOS Configurator
iOS configurator is a script designed to cut down certain manual configurations related to iOS. Click bellow to see the steps,
{{< expand "Steps" "..." >}}
1. Using command prompt, goto <strong>IOT_HOME/ios-configurator/</strong> folder
2. Press <strong>Enter</strong> key to provide the default value and provide the following mandatory information with the server machine's IP address:
<ul style="list-style-type:disc;">
    <li>Common Name </li>
    <li>Server address</li>
</ul>
{{< /expand >}}

## Customizing the IOS Bundle ID

If the bundle ID for the iOS agent is changed, the following changes have to be applied to the server configurations
{{< expand "Steps" "..." >}}
Open <strong>iot-server.sh</strong> file in <strong>IOT_HOME/bin</strong> folder and search for key <strong>-Dagent-bundle</strong> and add the agent bundle ID as the value.

Example
<strong>-Dagent-bundle="org.wso2.carbon.emm.mdmagent"</strong>
{{< /expand >}}

## Create MDM APNS Certificate
Communication from server to device(OS MDM client) about new commands are delivered through Apple push notification service(APNS). To facilitate this, the IoT server needs to communicate with APNS servers and this requires a special certificate.
In order to create an MDM APNS certificate, <strong>one of the following</strong> is needed,
<ul style="list-style-type:disc;">
    <li>MDM vendor signing certificate which allow an EMM vendor to sign MDM APNS certificates for their customers</li>
    <li>Get an MDM vendor to provide you with an MDM APNS certificate</li>
</ul>

{{< expand "Steps to get Entgra to provide you with an MDM APNS certificate" "..." >}}
### Get an MDM Vendor to Provide an MDM APNS Certificate

#### Step 1
This is the fastest approach and Entgra provides this server to valid customers. To do this, execute the following 2 commands on the commands prompt. The values provided when running these commands have no operation importance. 

```bash
openssl genrsa -des3 -out customerPrivateKey.pem 2048 
openssl req -new -key customerPrivateKey.pem -out customer.csr
```
#### Step 2
Keep the <strong>customerPrivateKey.pem</strong> file generate with above commands, secure along with the password used for private key. Email <strong>customer.csr</strong> file along with a description about the project details to <strong>contact@entgra.io</strong> to get a MDM APNS certificate. After an evaluation of the request, Entgra may send a file called plist_encoded

#### Step 3
Go to the Apple Push Certificate Portal at https://identity.apple.com/pushcert/ and log in with an Apple ID. It is highly recommended not to use personal Apple IDs for this process and to create a separate Apple ID for the organization. This Apple ID is needed when these certificate needs to be renewed and failure to access this Apple ID in the future will result in having to re-enrol all the devices in production.

<ul style="list-style-type:disc;">
    <li>If you are trying to renew the certificate, click <strong>Renew</strong>.</li>
    <li>If not, <strong>Click Create Certificate</strong> and agree to the terms and conditions.</li>
    <li>Upload the encoded plist_encoded file you received via email from Entgra.</li>
    <li>Download the generated MDM signing certificate (MDM_Certificate.pem). The MDM signing certificate is a certificate for 3rd party servers provided by Apple.</li>
</ul>

#### Step 4
Run the following command and note down the userID value in the output. This will later be indentified as the topic ID.

```bash
openssl x509 -in MDM_Certificate.pem -text -noout
```

#### Step 5
Run the following 2 commands,

```bash
openssl rsa -in customerPrivateKey.pem -out customerKey.pem
cat MDM_Certificate.pem customerKey.pem > MDM_APNSCert.pem
```

#### Step 6
Open the MDM Apple Push Notification service (APNs) Certificate (<strong>MDM_APNSCert.pem</strong>) and ensure that there is a line break between the contents of the two files.
Example:
The content will look as follows:-----END CERTIFICATE----------BEGIN RSA PRIVATE KEY-----
Therefore, add a line break to separate the 2 certificates after 5 - (dashes) so that the content will look like what's shown below:

```bash
-----END CERTIFICATE-----
-----BEGIN RSA PRIVATE KEY-----
```

#### Step 7
Run the following command to convert the <strong>MDM_APNSCert.pem</strong> file to the <strong>MDM_APNSCert.pfx</strong> file. You will need to provide a password when converting the file.
```bash
openssl pkcs12 -export -out MDM_APNSCert.pfx -inkey customerPrivateKey.pem -in MDM_APNSCert.pem
```

{{< /expand >}}

## Decide Your Enrollment Method

Before moving on to next steps, a decision needs to be made according to the features required for the business usecase. As mentioned previously, iOS and MacOS has an in-built MDM client which is capable of performing majority of management tasks. However the agent is needed, if the business requirement needs the following feature,

<ul style="list-style-type:disc;">
    <li>Location tracking</li>
    <li>Ring the device</li>
    <li>Send a short notification message to device</li>
</ul>

If the above 3 features are not mandatory, please skip ahead to <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/apple-server-configurations/#add-platform-configurations"> 
adding Platform Configurations</a> section.

## Create Agent APNS Certificate

This certificate is required to carry out operations on the device that need to be triggered via the iOS agent, such as ringing the device, getting the device location, and sending notifications or messages to the device. Therefore, if you are not installing the iOS agent on your devices, you do not need this certificate and this section can be skipped.

{{< expand "Steps" "..." >}}
{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li>You have to be enrolled in the <a href="https://developer.apple.com/programs/"> Apple Developer Program</a> as an individual or an organization before starting the iOS server configurations.</li>
    <li>A valid distribution certificate that you obtained from Apple.</li>
</ul>
{{< /hint >}}

### Step 1

Clone the <a href="https://gitlab.com/entgra/ios-agent">ios-agent repository</a> to a preferred location.
```bash
git clone https://gitlab.com/entgra/ios-agent
```

### Step 2

Open the <strong>emm-agent-ios</strong> from X-Code and follow the subsequent steps:

1. Change the <strong>org.wso2.carbon.emm.mdmagent</strong> Bundle Identifier to a value prefered.
Example: <strong>org.<ORGANIZATION_NAME>.emm.ios.agent</strong>
2. Select the development team, provisioning profile and sign certificate from Xcode.

If you are unsure of how to select the development team, or adding the provisioning profile or signing the certificate via Xcode, see the blog post on <a href="https://medium.com/@mharindu/how-to-export-in-house-developed-ios-app-as-an-enterprise-application-dc087bdd64c3#.ptkvra6o3">How to export “in-house” developed iOS app as an enterprise application</a>.

### Step 3

Log in to the <a href="https://developer.apple.com/account">Apple Developer program</a> and follow the subsequent steps:

Before you follow the steps, confirm that your machine is connected to the Internet and that Xcode has a valid developer account.

Navigate to Certificates, IDs & Profiles that is under Identifiers.
 Click App IDs and see if the Bundle ID that you defined under Xcode is listed here.

<img src = "../../../../../image/2006.png" style="border:5px solid black ">

### Step 4

Click the Bundle ID, and click <strong>Edit</strong>.

<img src = "../../../../../image/2007.png" style="border:5px solid black ">

### Step 5

Creating an APNs SSL certificate:

#### Step 1. Select Push Notifications to enable the setting.

<img src = "../../../../../image/2008.png" style="border:5px solid black ">

Once push notification is enabled, you are able to generate the development and production certificates.

#### Step 2. To try out the create certificate use case, let us create a development SSL certificate.
Please note that the development SSL certificate is created only as an example. You can create a production SSL certificate if you have registered with the Apple Developer Program as an Organization.

Click <strong>Create Certificate</strong> that is under Development SSL Certificate.

<img src = "../../../../../image/2009.png" style="border:5px solid black "> 

### Step 6

Creating a CSR file using the keychain access tool in the Mac OS:

#### Step 1. Launch the keychain access application.

#### Step 2. On the menu bar click KeyChain Access > Certificate Assistant > Request a Certificate from Certificate Authority.

<img src = "../../../../../image/2010.png" style="border:5px solid black ">

#### Step 3. Define the email address, common name, select Saved to disk, and click Continue.
Example:

<img src = "../../../../../image/2011.png" style="border:5px solid black "> 

### Step 7

Go back to the Apple Developer Portal, upload the generated certificate, and click <strong>Continue</strong>.

<img src = "../../../../../image/2012.png" style="border:5px solid black ">

### Step 8

Exporting the certificate to the pfx format.
1. Click <strong>Download</strong> to download the file.

<img src = "../../../../../image/2013.png" style="border:5px solid black "> 

2. Double-click the downloaded file to open it with the Keychain access tool.
3. Right-click the certificate and select export.
4. Define the location where you wish to save the file and set a password for the exported file when prompted.
5. Rename the p12 extension of the file to pfx.

{{< /expand >}}


## Add Platform Configurations

Before proceeding, note, if you require Apple DEP support, please complete  <a href="../apple-DEP-configurations"> Apple DEP configurations</a> section first.

{{< expand "Steps" "..." >}}
Log in to the device management console and Click  > CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS > iOS Configurations and fill in the form with values you wish.

Use the generated MDM-APNS certificate for both the MDM certificate and the generated APNS certificate for the APNS certificate.

MDM Certificate: Upload the MDM APNS pfx file created(MDM_APNSCert.pfx)
MDM Certificate Password: Give the same password you gave when converting the MDM_APNS certificate from the pem to the pfx format. 
MDM Certificate Topic ID: Give the topic ID of the certificate. If you are unsure of the topic ID, refer step 6 under generating the MDM APNS certificate.

APNS Certificate: If you are using Agent and completed <a href="#create-agent-apns-certificate"> Create agent APNS certificate</a> section, please use that pfx. If not upload the MDM_APNSCert.pfx here as well
APNS Certificate Password: Give the same password you gave when converting the Agent APNS certificate to the pfx format. If Agent is not used, provide the same password as MDM_APNSCert.pfx


<img src = "../../../../../image/2014.png" style="border:5px solid black "> 


<strong>Tip:</strong> To learn more about each platform setting, hover your mouse pointer over the help tip.

{{< /expand >}}




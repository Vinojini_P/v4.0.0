---
bookCollapseSection: true
weight: 3
---
# Assigned Access Policy

   Kiosk Mode that allows a user to lock on to a single app. It requires the users to enter a user name and a domain, the domain is optional if the user name is unique across the system. Moreover, it requires an AMUID which points to the app that will be running on that user. Steps to finding the AUMID can be found here. It's important to keep in mind if there is a single user when the policy is being applied, there is no way to access the settings and manually sync and revoke the policy. The IoT server sends requests every one minute and if there are any pending revoke policies it will sync at that time. Alternatively, the device could be manually synced using another user account and the changes will be applied. A computer restart is required for the policy enforcement and revoke to take effect.

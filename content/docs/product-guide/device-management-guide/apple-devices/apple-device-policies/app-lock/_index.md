---
bookCollapseSection: true
weight: 17
---

# App Lock (Kiosk)


{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configuration can be used to enforce the iOS device to a single application i.e to make a device act as a Kiosk.

<i>This configuration will be applied only on <strong>Supervised devices</strong> having <strong>iOS 7.0 and later</strong>.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Identifier</strong></td>
            <td> The bundle identifier of the application.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Options</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Disable touch</strong></td>
            <td>If true, the touch screen is disabled.</td>
        </tr>
        <tr>
            <td><strong>Disable Device Rotation</strong></td>
            <td>If true, device rotation sensing is disabled</td>
        </tr>
        <tr>
            <td><strong>Disable volume buttons</strong></td>
            <td>If true, the volume buttons are disabled.</td>
        </tr>
        <tr>
            <td><strong>Disable ringer switch</strong></td>
            <td>If true, the ringer switch is disabled.</td>
        </tr>
        <tr>
            <td><strong>Disable sleep wake button</strong></td>
            <td>If true, the sleep/wake button is disabled.</td>
        </tr>
        <tr>
            <td><strong>Disable auto lock</strong></td>
            <td>If true, the device will not automatically go to sleep after an idle period.</td>
        </tr>
        <tr>
            <td><strong>Enable voice over</strong></td>
            <td>If true, VoiceOver is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable zoom</strong></td>
            <td>If true, Zoom is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable invert colors</strong></td>
            <td>If true, Invert Colors is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable assistive touch</strong></td>
            <td>If true, AssistiveTouch is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable speak selection</strong></td>
            <td>If true, Speak Selection is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable mono audio</strong></td>
            <td>If true, Mono Audio is turned on.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>User Enabled Options</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Voice over</strong></td>
            <td>If true, allow VoiceOver adjustment.</td>
        </tr>
        <tr>
            <td><strong>Zoom</strong></td>
            <td>If true, allow Zoom adjustment.</td>
        </tr>
        <tr>
            <td><strong>Invert colors</strong></td>
            <td>If true, allow Invert Colors adjustment.</td>
        </tr>
        <tr>
            <td><strong>Assisstive touch</strong></td>
            <td>If true, allow AssistiveTouch adjustment.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

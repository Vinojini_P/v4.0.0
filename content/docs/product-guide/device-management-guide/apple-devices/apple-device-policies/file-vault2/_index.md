---
bookCollapseSection: true
weight: 21
---

# FileVault 2 Policy

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

In macOS 10.9, you can use FileVault 2 to perform full XTS-AES 128 encryption on the contents of a volume.

<table style="width: 100%;">
    <colgroup>
        <col />
        <col />
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Enable Firewall</strong></td>
            <td>Set to ʼOnʼ to enable FileVault. Set to ʼOffʼ to disable FileVault. This value is required.</td>
        </tr>
        <tr>
            <td><strong>Defer</strong></td>
            <td>Set to true to defer enabling FileVault until the designated user logs out. The person enabling FileVault must be either a local user or a mobile account user.</td>
        </tr>
        <tr>
            <td><strong>User Enters Missing Info</strong></td>
            <td>Set to true for manual profile installs to prompt for missing user name or password fields.</td>
        </tr>
        <tr>
            <td><strong>Use Recovery Key</strong></td>
            <td>Set to true to create a personal recovery key. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Show Recovery Key</strong></td>
            <td>Set to false to not display the personal recovery key to the user after FileVault is enabled.</td>
        </tr>
        <tr>
            <td><strong>Show Recovery Key</strong></td>
            <td>Set to false to not display the personal recovery key to the user after FileVault is enabled.</td>
        </tr>
        <tr>
            <td><strong>Output Path</strong></td>
            <td>Path to the location where the recovery key and computer information plist will be stored.</td>
        </tr>
        <tr>
            <td><strong>Certificate</strong></td>
            <td>DER-encoded certificate data if an institutional recovery key will be added.</td>
        </tr>
        <tr>
            <td><strong>Payload Certificate UUID</strong></td>
            <td>UUID of the payload containing the asymmetric recovery key certificate payload.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>User name of the Open Directory user that will be added to FileVault.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>User password of the Open Directory user that will be added to FileVault.</td>
        </tr>
        <tr>
            <td><strong>Use Keychain</strong></td>
            <td>If set to true and no certificate information is provided in this payload, the keychain already created at /Library/Keychains/FileVaultMaster.keychain will be used when the institutional recovery key is added.</td>
        </tr>
        <tr>
            <td><strong>Defer Force At User Login Max Bypass Attempts</strong></td>
            <td>When using the Defer option you can optionally set this key to the maximum number of times the user can bypass enabling FileVault before it will require that it be enabled before the user can log in. If set to 0, it will
                always prompt to enable FileVault until it is enabled, though it will allow you to bypass enabling it. Setting this key to –1 will disable this feature. 
                <i>Availability: Available in macOS 10.10 and later.</i>
            </td>
        </tr>
        <tr>
            <td><strong>Defer Dont Ask At User Logout</strong></td>
            <td>When using the Defer option, set this key to true to not request enabling FileVault at user logout time.
                <i>Availability: Available in macOS 10.10 and later.</i>
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

---
bookCollapseSection: true
weight: 20
---

# Firewall Policy

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

A Firewall policy manages the Application Firewall settings that are accessible in the Security Preferences pane. This policy is available in macOS 10.12 and later.

<i>This configuration will be applied only on macOS devices.</i>

The ”Automatically allow downloaded signed software” and ”Automatically allow built-in software” options are not supported, but both will be forced ON when this payload is present.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Enable Firewall</strong></td>
            <td>Whether the firewall should be enabled or not.
            </td>
        </tr>
        <tr>
            <td><strong>Block all incoming connections</strong></td>
            <td>Corresponds to the “Block all incoming connections” option. When it is enabled incoming new connections are blocked</td>
        </tr>
        <tr>
            <td><strong>Enable stealth mode.</strong></td>
            <td>Corresponds to “Enable stealth mode.” When stealth mode is turned on, your Mac does not respond to “ping” requests and does not answer connection attempts from a closed TCP or UDP network.
            </td>
        </tr>
        <tr>
            <td colspan=2><center><strong>Applications</strong><br>The list of applications. Each dictionary contains these keys:</center></td>
        </tr>
        <tr>
            <td><strong>Bundle ID</strong></td>
            <td>Identifies the application. It should be a string value.
            </td>
        </tr>
        <tr>
            <td><strong>Allowed</strong></td>
            <td>Specifies whether or not incoming connections are allowed
            </td>
        </tr>             
    </tbody>
</table>


{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

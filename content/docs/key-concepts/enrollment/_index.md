---
bookCollapseSection: true
weight: 2
---

# Enrollment

Process of on-boarding a new device to the server is called Enrollment. This process often involve the end-use typing in the Username and the password in an agent application or scan a QR code through the agent application to authenticate the user and enroll with the server. There are multiple says on performing the enrollment and depending on the method used to enroll, the operations that can be executed on a device varies.

## Android

There are number of different way an Android device can be enrolled on to the IoT server. This mainly depends on if the device is a BYOD device or a COPE device. Since the BYOD devices are owned by the user that brings it in, the level of control and information retrieval from the device has to be done minimum. But the COPE devices are organization owned and it is possible to execute any type of commons on top of these devices. This distinction of access level is provided by Android to EMM vendors via the mechanism used to enroll the agent. For example, to enroll a COPE device, the device has to be either a new device which has not been booted before or a factory resettled device. Hence, the user owned devices cannot be made as COPE as this requires a factory reset. Bellow diagram explains this distinction in more granular level.

<img src = "../../image/android-enrollment-type.png" style="border:5px solid black ">

### Work profile
A work profile create a containerized space in the device where the user data and apps are seperated from the wor apps and data, providing a clear separation in data and management. This is the recommended enrollment type for BYOD scenarios as user's data cannot be access by the enterprise admins which protects the privacy of user. At the same time, the corporate data resides in a secured container, preventing it from leaking out.

### Legacy
This is a deprecated form of enrollment where the user's personal space is enrolled with the server and the admin can manage the whole device instead of a container.

### Fully managed
These are corporate owned devices that are given to employees. Since the device ownership is with the organization, the level of restrictions and control is managed by the admin. The admin will have unrestricted access over these devices and the device can only be put to this mode after a factory reset or on a newly bought device.

### Dedicated devices(Kiosk)
A device dedicated to do a dedicated task(app) or a set of tasks(apps) is known as a dedicated device. Most common example of a dedicated device is a Kiosk terminal such as an ATM or a vending machine that only runs one app and the user is not allowed to perform any other task. Similar to fully managed the devices, dedicated devices are owned by the admin and the enrollment is identical to a fully managed device.

## iOS
Apple devices have a built-in agent that controls the device and the EMM servers has to implement the protocol defined by Apple to managed devices. A special profile is installed on the device as part of the enrolment which defines what are the endpoint the device needs to communicate to, enroll, check-in, fetch commands and respond. These endpoints are standard endpoints common to all EMMs and, the EMM vendor implements these end points based on the protocol Apple has provided. IoT server has such an implementation to enroll COPE and BYOD devices and manage the devices and their data.

<img src = "../../image/ios-enrollment-type.png" style="border:5px solid black ">

### User enrollment
User enrollment is similar to work profiles in Android where a containerized space is provided for the work apps to exist and the EMM admin only has access to the data and apps that resides in this container. Device level operations and policies such as lock, wipe, network proxy configuration are not allowed. User enrollment is purely for enterprises that want to enable BYOD and have data security while protecting privacy of the user.

### Device enrollment
This is the standard enrollment of the whole device where the user installs few profiles and enroll with the server.

### DEP
DEP or the device enrollment program is a mechanism Apple provides to enroll COPE device with more privilages granted to the EMM admin to better manage organizations devices. DEP enrolled device can belong to one of the bellow 2 types based on the polices added to it.

### Fully managed
These are DEP devices used as employee's devices where the organization has full control over. The user will have to all the standard iOS features and apps depending on the policies EMM admin enforces.

### Dedicated
Device can be restricted to a single app to make the device a kiosk. This enrollment is same as fully managed enrollment. The admin can add a policy to make a kiosk out of the device, which makes this a dedicated device.
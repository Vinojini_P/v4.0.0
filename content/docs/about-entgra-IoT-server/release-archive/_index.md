---
weight: 4
title: "Release Archive"
---

# Release Archive

This section features each successive version of the Entgra IoT server in descending order as per the release timeline. 

Entgra IoT 3.4.0 is independently released as a successor to, and based on  <a target="_blank" href="https://docs.wso2.com/display/IoTS331/WSO2+IoT+Server+Documentation">WSO2 IoT Server 3.3.1</a>, proceeding from then onwards as Entgra releases. 


### <strong>Entgra IoTs 3.8.0</strong>
_Release Date: 07th November, 2019_
<div>
<ul style="list-style-type:disc;">

<li>Corrective policy for policy violations</li>
<li>Defining policy type (General/corrective)</li>
<li>Fully rewritten App manager for performance improvement</li>
<li>Scheduled webclip/enterprise/public app uninstallments</li>
<li>Adding custom apps for IoT devices</li>
<li>Enable/Disable lock task mode</li>
<li>Display extensive device information</li>
<li>Windows PC support added to the platform</li>
<li>macOS support added to the platform</li>
<li>macOS App installation API level support</li>
<li>Windows PC Installation Apps API level support</li>
</ul>
</div>

### Entgra IoTs 3.7.0

_Release Date: 26th October, 2019_

<div>
<ul style="list-style-type:disc;">
<li>[Android] Ability to turn off or on backing up data</li>
<li>[Android] Automate enrollment by using QR code</li>
<li>[Android] [iOS] New operation to add Android/iOS application configuration</li>
<li>[iOS] New policy to install fonts</li>
<li>[iOS] Ability to control voice roaming</li>
<li>[iOS] Ability to control Data roaming</li>
<li>[iOS] Ability to control WiFi hotspot</li>
<li>[iOS] Ability to control bluetooth</li>
<li>[iOS] Ability to change wallpaper</li>
<li>[iOS] Ability to install macOS Applications</li>
<li>[iOS] New policy to add network usage rules</li>
<li>[iOS] Active Sync integration</li>
<li>[iOS] New policy to restrict the app store of macOS device</li>
<li>[iOS] Ability to create managed preferences on macOS device</li>
<li>[iOS] Ability to shut down macOS device</li>
<li>[iOS] Ability to restart macOS device</li>
<li>[Windows] Disable Adding Non-Microsoft Accounts Manually</li>
<li>[Windows] Disable Private Browsing</li>
<li>[Windows] Disable Removable Drive Indexing</li>
<li>[Windows] Restricts the users from changing language settings</li>
<li>[Windows] Disables region settings on a device</li>
<li>[Windows] Disables Cortana from the Windows search bar</li>
</ul>
</div>

### Entgra IoTs 3.6.0

_Release Date: 09th July, 2019_

<div>
<ul style="list-style-type:disc;">
<li>[Android] Ability to add third-party applications as VPN providers for Android devices (Always on VPN)</li>
<li>[Android] Ability to measure data usage in Devices</li>
<li>[Android] Ability to configure third-party applications remotely in Devices</li>
<li>[Android] New Restrictions for Android Devices in Device Owner Mode</li>
<li>[Android] Disable setting wallpapers</li>
<li>[Android] Disable Bluetooth</li>
<li>[Android] Disable Bluetooth sharing</li>
<li>[Android] Disable data roaming</li>
<li>[Android] Disable set user icon</li>
<li>[Android] Disable remote management profile</li>
<li>[Android] Disableremove user</li>
<li>[Android] Disable autofill</li>
<li>[Android] Improvements to Kiosk mode</li>
<li>[Android] New API to notify agents that a server upgrade has taken place</li>
<li>[Android] [iOS] Install certificates in devices</li>
<li>[iOS] Support for Kiosk mode</li>
<li>[iOS] New API to install/remove external profiles on Apple devices.</li>
<li>Ability to add operations to groups/lists of devices</li>
<li>New API to permanently delete devices</li>
<li>New API to query devices based on device properties</li>
<li>Multiple bug fixes and improvements in UI and APIs</li>
</ul>
</div>

### Entgra IoTS 3.5.0

_Release Date: 24th April, 2019_

<div>
<ul style="list-style-type:disc;">
<li>[Android] Policy to install apps at device enrollment time</li>
<li>[Android] Policy to configure global HTTP Proxy on devices</li>
<li>[Android] QR code based Kiosk and COPE device enrollment</li>
<li>[Android] Multi-app and single app Kiosk support</li>
<li>[Android] Policy for custom theming and idle timeout videos support for Kiosks</li>
<li>[Android] User session support for Kiosk (shared device)</li>
<li>[Android] Remote configurable secure browser support for Kiosk</li>
<li>[Android] Open VPN configuration policy for Android</li>
<li>[iOS] Application block-listing and allow-listing support</li>
<li>[Android] [iOS] Background Enrollment triggering from external apps</li>
<li>[Android] Android TV OS support</li>
<li>Notify enrollment complete status to external systems</li>
<li>Multiple bug fixes and improvements in UI and APIs</li>
</ul>
</div>

### Entgra IoTS 3.4.0

_Release Date: 11th November, 2018_

Entgra IoT Server version 3.4.0 is the successor of <a target="_blank" href="https://docs.wso2.com/display/IoTS331/WSO2+IoT+Server+Documentation">WSO2 IoT Server 3.3.1</a>.

<div>
<ul style="list-style-type:disc;">

<li>Device Enrollment Program (DEP) for iOS devices.</li>
<li>Making Entgra IoT Server 3.3.0 General Data Protection Regulations (GDPR) compliant.</li>
<ul style="list-style-type:disc;">
<li>Removing user and device details when the user requests to be forgotten.</li>
<li>Consent management when signing in to Entgra IoT Server.</li>
<li>Consent management when enrolling devices.</li>
<li>Introduction of cookie policies and privacy policies in the device management console.</li>
</ul>
</ul>
</div>

### Resources

<div>
<ul style="list-style-type:disc;">
<li>WSO2Con Asia 2016 - WSO2 IoT Server: Your Foundation for the Internet of Things:
    <a href="https://www.youtube.com/watch?v=8ZBSdbvWxKc&feature=emb_title" target="_blank">https://www.youtube.com/watch?v=8ZBSdbvWxKc&feature=emb_title</a></li>
</ul>
</div>
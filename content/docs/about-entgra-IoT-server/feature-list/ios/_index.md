---
bookCollapseSection: true
weight: 2
title: "iOS & macOS"
---

# iOS Features

## Supported Operations

Entgra IoT Server facilitates one time <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Operations</a> that can be performed remotely via the Device Management Console. These operations are useful for runtime maintenance of devices.

Available operations for iOS and macOS vary according to how Apple defines the protocol and also depending on whether the device is a BYOD or a DEP (COPE) device.

The table below depicts a summary of this information. 

<table class="tableizer-table" border="1">
<tr aria-rowspan="2"><thead bgcolor="#9bc9f1"><th></th><th colspan="2">iOS</th><th>macOS </th></tr></thead><tbody>
 <tr><th bgcolor="#9bc9f1"><font style="Tahoma" size="2">Feature - Description</th><th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th><th bgcolor="#9bc9f1"><font style="Tahoma" size="2">DEP</th><th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th></tr>

 <tr><td><font style="Tahoma" size="2">Get Device Information - Fetch the device's runtime information.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Installed Applications - Fetch the device's installed application list.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Device Location Information - Fetch the device's current location.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Ring Device - Ring the device for the purpose of locating the device in case of misplace.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Install/Uninstall/update applications - Manage apps installed on the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Enterprise data wipe - Wipe the entreprise portion of the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Lock Device - Lock the device remotely. Similar to pressing the power button on the device and locking it.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Send Notification - Send a notification(message) to the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Clear Passcode - Clear the passcode of a mobile device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Wipe Device(factory reset) - Factory reset a device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Restart - Restart command is issued to restart the device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Shutdown - Shutdown command is issued to shutdown the device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">External profile add and remove APIs</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 </tr>
  <tr>
  <td><font style="Tahoma" size="2">Fetch security information of devices.</td>
  <td align="center"><font style="Tahoma" size="2">✓</td>
  <td align="center"><font style="Tahoma" size="2">✓</td>
  <td align="center"><font style="Tahoma" size="2">✕</td>
  </tr>


</tbody></table>


## Policies

The policies that can be applied on an iOS device depends on the way the device is enrolled with the server.

Accordingly, the table below indicates the policies applicable for each type of enrollment. 

<table class="tableizer-table" border="1">
<tr aria-rowspan="2">
<thead bgcolor="#9bc9f1"><th></th><th colspan="2">iOS</th><th>macOS </th></thead>
</tr>
<tbody>
 <tr>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">Feature - Description</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">DEP</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th>

 <tr>
 <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/virtual-private-network/">Virtual Private Network (VPN Settings) - Push a configuration contaning the VPN profile of the company.</a></td>
<td align="center"><font style="Tahoma" size="2">✓</td>
<td align="center"><font style="Tahoma" size="2">✓</td>
<td align="center"><font style="Tahoma" size="2">✓</td>
          
  </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/wi-fi-policy/">Wi-Fi Settings - Push a configuration contaning the wifi profile of the company.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/calender/">Calendar - This payload configures a CalDAV account.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
          </tr>
        
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/calendar-subscription/">Calendar Subscription - Adds a subscribed calendar to the userʼs calendars list.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
         </tr>
                
            
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/cellular-network-settings/">Cellular Network Settings - Push cellular configurations such as APN settings to a mobile device.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                 </tr>
        
        
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/email-settings/">Email settings - These configurations can be used to define settings for connecting to your POP or IMAP email accounts.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
                   </tr>
                   
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/l-d-a-p-settings/">LDAP Settings - These configurations can be used to define settings for connecting to your LDAP server.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
                   </tr>
                   
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/manage-domains/">Manage Domains - Any document downloaded from the given URLs are marked as managed documents and will be used in managed open in restrictions.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
    
</tr>
          
          
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Unmarked Email Domains - Specify a list of email domains that are enterprise recognised so that the others are marked as unregnised by highlighting in the mail client.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
           </tr>
        
        
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/passcode-policy/">Passcode policy - Add a passcode strength policy to the device or to work profile.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Monitor/Revoke Policies - Continiously monitor the policies of the device to detect any policy violations.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
          </tr>
          
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}"product-guide/device-management-guide/apple-devices/apple-device-policies/certificate-install/>Certificate install - Install certificate to devices remotely.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">&nbsp;</td>
                  </tr>
                  
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}"product-guide/device-management-guide/apple-devices/apple-device-policies/global-proxy-settings/>Global Proxy Settings - Reroute all the http communication of a device via a global http proxy.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">&nbsp;</td>
                   </tr>
                   
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Disallow removal of profile - Disable the user's ability to unenroll from EMM.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                   </tr>
                   
 <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/air-play-settings/">AirPlay Settings - The AirPrint payload adds AirPrint printers to the user's AirPrint printer list.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                    </tr>
                    
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/network-usage-rules/">Network Usage Rules - Network Usage Rules allow enterprises to specify how managed apps use networks, such as cellular data networks.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                    </tr>
                    
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/app-lock/">App Lock (Kiosk mode) - Configure the behaviour of the Kiosk.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                 </tr>
                 
 <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/font-install/">Font install - Install fonts to an iOS device remotely.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
                </tr>
                
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Exchange - Exchange active sync contacts and mails to devices.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            </tr>
            
<tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Managed Settings command - Send the app configurations for user's installed apps.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
                 </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">AppStore Payload - Enforce restrictions on the App store in macOS.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
               </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Loginwindow Payload - Behaviour of the login screen and users are controlled with this policy.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
                </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/firewall-policy/">Firewall Policy - A Firewall payload manages the Application Firewall settings that are accessible in the Security Preferences pane.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
                  </tr>
    </tbody>
</table>

###  Restrictions Policy 

<a target="_blank" href="{{< param doclink>}}product-guide/device-management-guide/apple-devices/apple-device-policies/restrictions/">Restrictions Policies</a> are those that can be applied on a device restricting or controlling the use of certain specific device features. 

There are a large number of restrictions that can be applied on an iOS device. The following table lists the available Restriction Policies for iOS devices. 

<table class="tableizer-table" border="1">
<tr aria-rowspan="2">
<thead bgcolor="#9bc9f1"><th></th><th colspan="2">iOS</th><th>macOS </th></thead>
</tr>
<tbody>
 <tr>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">Feature - Description</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">DEP</th>
 <th bgcolor="#9bc9f1"><font style="Tahoma" size="2">BYOD</th>
 
 <tr><td><font style="Tahoma" size="2">allowAppRemoval - When false, disables removal of apps from iOS device. This key is deprecated on unsupervised devices.</td><td><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">✕</td></tr>
  
 <tr><td><font style="Tahoma" size="2">allowAssistant - When false, disables Siri. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowAssistantWhileLocked - "When false, the user is unable to use Siri when the device is locked. Defaults to true. This restriction is ignored if the device does not have a passcode set.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 5.1 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowCamera - "When false, the camera is completely disabled and its icon is removed from the Home screen. Users are unable to take photographs.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS and in macOS 10.11 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowCloudDocumentSync - "When false, disables document and key-value syncing to iCloud. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 5.0 and later and in macOS 10.11 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowCloudKeychainSync - "When false, disables iCloud keychain synchronization. Default is true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 7.0 and later and macOS 10.12 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowDiagnosticSubmission - "When false, this prevents the device from automatically submitting diagnostic reports to Apple. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 6.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowExplicitContent - When false, explicit music or video content purchased from the iTunes Store is hidden. Explicit content is marked as such by content providers, such as record labels, when sold through the iTunes Store. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowFingerprintForUnlock - "If false, prevents Touch ID from unlocking a device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 7 and later and in macOS 10.12.4 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowGlobalBackgroundFetchWhenRoaming - When false, disables global background fetch activity when an iOS phone is roaming.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowLockScreenNotificationsView - "If set to false, the Notifications view in Notification Center on the lock screen is disabled and users can't receive notifications when the screen is locked.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowLockScreenTodayView - "If set to false, the Today view in Notification Center on the lock screen is disabled.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowMultiplayerGaming - When false, prohibits multiplayer gaming. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowOpenFromManagedToUnmanaged - "If false, documents in managed apps and accounts only open in other managed apps and accounts. Default is true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowOpenFromUnmanagedToManaged - "If set to false, documents in unmanaged apps and accounts will only open in other unmanaged apps and accounts. Default is true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowScreenShot - "If set to false, users can't save a screenshot of the display and are prevented from capturing a screen recording; it also prevents the Classroom app from observing remote screens. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 
 </tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Updated in iOS 9.0 to include screen recordings."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowPhotoStream - "When false, disables Photo Stream.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 5.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowSafari - When false, the Safari web browser application is disabled and its icon removed from the Home screen. This also prevents users from opening web clips. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">safariAllowAutoFill - When false, Safari auto-fill is disabled. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">safariForceFraudWarning - When true, Safari fraud warning is enabled. Defaults to false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">safariAllowJavaScript - When false, Safari will not execute JavaScript. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">safariAllowPopups - >When false, Safari will not allow pop-up tabs. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">safariAcceptCookies - "Determines conditions under which the device will accept cookies.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">The user facing settings changed in iOS 11, though the possible values remain the same:</td></tr>
 <tr><td><font style="Tahoma" size="2">allowSharedStream - "If set to false, Shared Photo Stream will be disabled. This will default to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowUntrustedTLSPrompt - "When false, automatically rejects untrusted HTTPS certificates without prompting the user.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 5.0 and later."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowVideoConferencing - When false, disables video conferencing. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowVoiceDialing - When false, disables voice dialing if the device is locked with a passcode. Default is true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">allowYouTube - "When false, the YouTube application is disabled and its icon is removed from the Home screen.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 <tr><td><font style="Tahoma" size="2">This key is ignored in iOS 6 and later because the YouTube app is not provided."</td></tr>
 <tr><td><font style="Tahoma" size="2">allowiTunes - When false, the iTunes Music Store is disabled and its icon is removed from the Home screen. Users cannot preview, purchase, or download content. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowEnterpriseAppTrust - "If set to false removes the Trust Enterprise Developer button in Settings->General->Profiles & Device Management, preventing apps from being provisioned by universal provisioning profiles. This restriction applies to free developer accounts but it does not apply to enterprise app developers who are trusted because their apps were pushed via MDM, nor does it revoke previously granted trust. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 
 </tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td></tr>
 
 <tr><td><font style="Tahoma" size="2">forceEncryptedBackup - When true, encrypts all backups.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>

 <tr><td><font style="Tahoma" size="2">forceITunesStorePasswordEntry - "When true, forces user to enter their iTunes password for each transaction.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 5.0 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">forceLimitAdTracking - "If true, limits ad tracking. Default is false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">forceAirPlayOutgoingRequestsPairingPassword - "If set to true, forces all devices receiving AirPlay requests from this device to use a pairing password. Default is false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>

 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.1 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">forceAirPlayIncomingRequestsPairingPassword - "If set to true, forces all devices sending AirPlay requests to this device to use a pairing password. Default is false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available only in Apple TV 6.1 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowActivityContinuation - If set to false, Activity Continuation will be disabled. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowEnterpriseBookBackup - If set to false, Enterprise books will not be backed up. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowEnterpriseBookMetadataSync - If set to false, Enterprise books notes and highlights will not be synced. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowCloudPhotoLibrary - "If set to false, disables iCloud Photo Library. Any photos not fully downloaded from iCloud Photo Library to the device will be removed from local storage.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later and in macOS 10.12 and later.  "</td></tr>

 <tr><td><font style="Tahoma" size="2">forceAirDropUnmanaged - "If set to true, causes AirDrop to be considered an unmanaged drop target. Defaults to false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td></tr>

 <tr><td><font style="Tahoma" size="2">forceWatchWristDetection - "If set to true, a paired Apple Watch will be forced to use Wrist Detection. Defaults to false.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 8.2 and later. "</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowAddingGameCenterFriends - When false, prohibits adding friends to Game Center. This key is deprecated on unsupervised devices.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowInAppPurchases - When false, prohibits in-app purchasing.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowLockScreenControlCenter - "If false, prevents Control Center from appearing on the Lock screen.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 7 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowOTAPKIUpdates - "If false, over-the-air PKI updates are disabled. Setting this restriction to false does not disable CRL and OCSP checks. Default is true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td>/tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowPassbookWhileLocked - "If set to false, Passbook notifications will not be shown on the lock screen.This will default to true.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later."</td></tr>
 
 <tr><td><font style="Tahoma" size="2">allowManagedAppsCloudSync - If set to false, prevents managed applications from using iCloud sync.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>

 <tr><td><font style="Tahoma" size="2">allowPodcasts - "Not available. Need DEP. If set to false, disables podcasts. Defaults to true.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 8.0 and later. "</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowDefinitionLookup - "Not available. Need DEP. If set to false, disables definition lookup. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 8.1.3 and later and in macOS 10.11.2 and later. "</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowPredictiveKeyboard - "Not available. Need DEP. If set to false, disables predictive keyboards. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 8.1.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAutoCorrection - "Not available. Need DEP. If set to false, disables keyboard auto-correction. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 8.1.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowSpellCheck - "Not available. Need DEP. If set to false, disables keyboard spell-check. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 8.1.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowMusicService - "Not available. Need DEP. If set to false, Music service is disabled and Music app reverts to classic mode. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3 and later and macOS 10.12 and later. "
 </td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowNews - "Not available. Need DEP. If set to false, disables News. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowUIAppInstallation - "When false, the App Store is disabled and its icon is removed from the Home screen. However, users may continue to use Host apps (iTunes, Configurator) to install or update their apps. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowKeyboardShortcuts - "If set to false, keyboard shortcuts cannot be used. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowPairedWatch - "If set to false, disables pairing with an Apple Watch. Any currently paired Apple Watch is unpaired and erased. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr><td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowPasscodeModification - "If set to false, prevents the device passcode from being added, changed, or removed. Defaults to true. This restriction is ignored by shared iPads.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowDeviceNameModification - <font style="Tahoma" size="2">"If set to false, prevents device name from being changed. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowWallpaperModification - "If set to false, prevents wallpaper from being changed. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAutomaticAppDownloads - "If set to false, prevents automatic downloading of apps purchased on other devices. Does not affect updates to existing apps. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowRadioService - "If set to false, Apple Music Radio is disabled. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">blacklistedAppBundleIDs - "If present, prevents bundle IDs listed in the array from being shown or launchable.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">whitelistedAppBundleIDs - "If present, allows only bundle IDs listed in the array from being shown or launchable.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
  
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3 and later."</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowNotificationsModification - "If set to false, notification settings cannot be modified. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowRemoteScreenObservation - "If set to false, remote screen observation by the Classroom app is disabled. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">This key should be nested beneath allowScreenShot as a sub-restriction. If allowScreenShot is set to false, it also prevents the Classroom app from observing remote screens. Availability: Available in iOS 9.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowDiagnosticSubmissionModification - "If set to false, the diagnostic submission and app analytics settings in the Diagnostics & Usage pane in Settings cannot be modified. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 9.3.2 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowBluetoothModification - "If set to false, prevents modification of Bluetooth settings. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 10.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowDictation - "If set to false, disallows dictation input. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 10.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">forceWiFiWhitelisting - "If set to true, the device can join Wi-Fi networks only if they were set up through a configuration profile. Defaults to false.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 10.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">forceUnpromptedManaged- ClassroomScreenObservation - "If set to true, and ScreenObservationPermissionModificationAllowed is also true in the Education payload, a student enrolled in a managed course via the Classroom app will automatically give permission to that course's teacher's requests to observe the student's screen without prompting the student. Defaults to false.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 10.3 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAirPrint - "If set to false, disallow AirPrint. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
 
<tr>
<td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td>
</tr>

<tr>
<td><font style="Tahoma" size="2">allowAirPrintCredentialsStorage - If set to false, disallows keychain storage of username and password for Airprint. Defaults to true.</td>
<td align="center"><font style="Tahoma" size="2">✕</td>
<td align="center"><font style="Tahoma" size="2">✓</td>
<td align="center"><font style="Tahoma" size="2">✕</td>
</tr>

<tr>
<td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and later. "</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">forceAirPrintTrustedTLSRequirement - If set to true, requires trusted certificates for TLS printing communication. Defaults to false.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAirPrintiBeaconDiscovery - If set to false, disables iBeacon discovery of AirPrint printers. This prevents spurious AirPrint Bluetooth beacons from phishing for network traffic. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowSystemAppRemoval - If set to false, disables the removal of system apps from the device. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowVPNCreation - If set to false, disallow the creation of VPN configurations. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowProximitySetupToNewDevice - Supervised only. If set to false, disables the prompt to setup new devices that are nearby. Defaults to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 11.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAccountModification - If set to false, account modification is disabled.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later. "</td>
 </tr>
  
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAppCellularDataModification - If set to false, changes to cellular data usage for apps are disabled.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAppInstallation - When false, the App Store is disabled and its icon is removed from the Home screen. Users are unable to install or update their applications. This key is deprecated on unsupervised devices.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowAssistantUserGeneratedContent - When false, prevents Siri from querying user-generated content from the web.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 7 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowBookstore - If set to false, iBookstore will be disabled. This will default to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowBookstoreErotica - "Not available prior to iOS 6.1. If set to false, the user will not be able to download media from the iBookstore that has been tagged as erotica. This will default to true.</td><td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowChat - "When false, disables the use of the Messages app with supervised devices.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later. "</td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowFindMyFriendsModification - "If set to false, changes to Find My Friends are disabled.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>

 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowGameCenter - "When false, Game Center is disabled and its icon is removed from the Home screen. Default is true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 6.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowHostPairing - "If set to false, host pairing is disabled with the exception of the supervision host. If no supervision host certificate has been configured, all pairing is disabled. Host pairing lets the administrator control which devices an iOS 7 device can pair with.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowUIConfigurationProfileInstallation - "If set to false, the user is prohibited from installing configuration profiles and certificates interactively. This will default to true.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available in iOS 6.0 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">autonomousSingleAppModePermittedAppIDs - "If present, allows apps identified by the bundle IDs listed in the array to autonomously enter Single App Mode.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">Availability: Available only in iOS 7.0 and later."</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">forceAssistantProfanityFilter - When true, forces the use of the profanity filter assistant.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowEraseContentAndSettings - If set to false, disables the "Erase All Content And Settings" option in the Reset UI.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td><font style="Tahoma" size="2"> </td></tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowSpotlightInternetResults - "If set to false, Spotlight will not return Internet search results.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
</tr>
 
 <tr>
 <td>
 
 <font style="Tahoma" size="2">Availability: Available in iOS and in macOS 10.11 and later. "</td>
 </tr>
 
 <tr>
 <td><font style="Tahoma" size="2">allowEnablingRestrictions - If set to false, disables the "Enable Restrictions" option in the Restrictions UI in Settings.</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
 <td align="center"><font style="Tahoma" size="2">✓</td>
 <td align="center"><font style="Tahoma" size="2">✕</td>
</tr>

</tbody></table>
